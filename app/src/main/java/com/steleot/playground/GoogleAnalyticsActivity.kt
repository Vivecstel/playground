package com.steleot.playground

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class GoogleAnalyticsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_analytics)
    }
}