package com.steleot.playground

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import kotlinx.android.extensions.CacheImplementation
import kotlinx.android.extensions.ContainerOptions

import kotlinx.android.synthetic.main.activity_main.*

@ContainerOptions(cache = CacheImplementation.SPARSE_ARRAY)
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        /* button listeners */
        androidKtx.setOnClickListener { startActivity(Intent(this, AndroidKtxActivity::class.java)) }

        materialComponents.setOnClickListener { startActivity(Intent(this, MaterialComponentsActivity::class.java)) }

        firebaseAuthentication.setOnClickListener { startActivity(Intent(this, AuthenticationActivity::class.java)) }

        bluetooth.setOnClickListener { startActivity(Intent(this, BluetoothActivity::class.java)) }

        firebaseCloudStorage.setOnClickListener { startActivity(Intent(this, CloudStorageActivity::class.java)) }

        constraintLayout.setOnClickListener { startActivity(Intent(this, ConstraintLayoutActivity::class.java)) }

        firebaseCrashlytics.setOnClickListener { startActivity(Intent(this, CrashlyticsActivity::class.java)) }

        firebaseFirestore.setOnClickListener { startActivity(Intent(this, FirestoreActivity::class.java)) }

        firebaseGoogleAnalytics.setOnClickListener { startActivity(Intent(this, GoogleAnalyticsActivity::class.java)) }

        gson.setOnClickListener { startActivity(Intent(this, GsonActivity::class.java)) }

        lifeCycles.setOnClickListener { startActivity(Intent(this, LifecyclesActivity::class.java)) }

        liveData.setOnClickListener { startActivity(Intent(this, LiveDataActivity::class.java)) }

        firebaseMlKit.setOnClickListener { startActivity(Intent(this, MlKitActivity::class.java)) }

        motionLayout.setOnClickListener { startActivity(Intent(this, MotionLayoutActivity::class.java)) }

        navigation.setOnClickListener { startActivity(Intent(this, NavigationActivity::class.java)) }

        networkConnectivity.setOnClickListener { startActivity(Intent(this, NetworkConnectivityActivity::class.java)) }

        okhttp.setOnClickListener { startActivity(Intent(this, OkhttpActivity::class.java)) }

        paging.setOnClickListener { startActivity(Intent(this, PagingActivity::class.java)) }

        retrofit.setOnClickListener { startActivity(Intent(this, RetrofitActivity::class.java)) }

        room.setOnClickListener { startActivity(Intent(this, RoomActivity::class.java)) }

        viewModel.setOnClickListener { startActivity(Intent(this, ViewModelActivity::class.java)) }

        workManager.setOnClickListener { startActivity(Intent(this, WorkManagerActivity::class.java)) }
    }
}
