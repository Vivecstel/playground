package com.steleot.playground

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.bluetooth.BluetoothAdapter
import android.support.design.widget.Snackbar
import kotlinx.android.synthetic.main.activity_bluetooth.*
import android.content.Intent
import android.view.View
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter

class BluetoothActivity: AppCompatActivity() {

    private val REQUEST_ENABLE_BT = 100
    private val REQUEST_ENABLE_BT_DISC = 101

    private var bluetoothAdapter: BluetoothAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)

        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        registerReceiver(receiver, filter)

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (bluetoothAdapter == null) {
            bluetooth.visibility = View.GONE
            Snackbar.make(parentView, R.string.not_supporting_bluetooth, Snackbar.LENGTH_LONG).show()
        } else {
            bluetoothAdapter?.let { adapter ->
                setText(adapter.isEnabled)
                query(adapter.isEnabled)
                discoverability(adapter.isEnabled)
                bluetooth.setOnClickListener {
                    if (adapter.isEnabled) {
                        adapter.disable()
                        setText(false)
                        query(false)
                        discoverability(false)
                        text.text = ""
                    } else {
                        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                    }
                }
            }
        }
    }

    private fun setText(isEnabled: Boolean) {
        bluetooth.setText(if (isEnabled) R.string.disableBluetooth else R.string.enableBluetooth)
    }

    private fun discoverability(isEnabled: Boolean) {
        if (isEnabled) {
            discoverability.visibility = View.VISIBLE
            discoverability.setOnClickListener {
                val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
                startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT_DISC) // it returns result code the duration. in this case 300
            }
        } else {
            discoverability.visibility = View.GONE
        }
    }

    private fun query(isEnabled: Boolean) {
        if (isEnabled) {
            query.visibility = View.VISIBLE
            query.setOnClickListener {
                queryPairedDevices(bluetoothAdapter)
            }
        } else {
            query.visibility = View.GONE
            query.setOnClickListener(null)
        }
    }

    private fun queryPairedDevices(bluetoothAdapter: BluetoothAdapter?) {
        bluetoothAdapter?.let {
            val pairedDevices = it.getBondedDevices()

            val sb = StringBuilder()
            if (pairedDevices.size > 0) {
                // There are paired devices. Get the name and address of each paired device.
                for (device in pairedDevices) {
                    sb.append("device name : ${device.name}\n") // Device name
                    sb.append("mac address : ${device.address}\n") // MAC address
                }
            }

            text.text = sb
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when(requestCode) {
                REQUEST_ENABLE_BT -> {
                    setText(true)
                    query(true)
                    discoverability(true)
                }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver);
    }

    private val receiver = object: BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                val action = it.getAction()
                if (BluetoothDevice.ACTION_FOUND == action) {
                    // Discovery has found a device. Get the BluetoothDevice object and its info from the Intent.
                    val device = it.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice
                    Snackbar.make(parentView,
                            "device name : ${device.name} mac address : ${device.address} found",
                            Snackbar.LENGTH_LONG).show()
                }
            }

        }
    }
}