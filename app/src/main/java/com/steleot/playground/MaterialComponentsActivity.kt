package com.steleot.playground

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MaterialComponentsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material_components)
    }
}