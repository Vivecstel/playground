package com.steleot.playground

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import kotlinx.android.synthetic.main.activity_network_connectivity.*
import android.net.Network
import android.net.NetworkRequest

class NetworkConnectivityActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_network_connectivity)

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder = NetworkRequest.Builder()

        connectivityManager.registerNetworkCallback(
                builder.build(),
                object : ConnectivityManager.NetworkCallback() {

                    override fun onAvailable(network: Network) {
                        val snackbar = Snackbar.make(parentView,
                                R.string.network_connected, Snackbar.LENGTH_LONG)
                        snackbar.view.setBackgroundColor(Color.GREEN)
                        snackbar.show()
                    }

                    override fun onLost(network: Network) {
                        val snackbar = Snackbar.make(parentView, R.string.network_not_connected,
                                Snackbar.LENGTH_LONG)
                        snackbar.view.setBackgroundColor(Color.RED)
                        snackbar.show()
                    }
                }

        )
    }
}